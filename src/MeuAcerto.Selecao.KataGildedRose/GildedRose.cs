﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        private const string IngressosConcertoTuristas = "Ingressos para o concerto do Turisas";
        private const string QueijoBrie = "Queijo Brie Envelhecido";
        private const string DenteTarrasque = "Dente do Tarrasque";
        private const string ItemConjurado = "Conjurado";

        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            foreach (Item item in Itens)
            {
                if (item.Nome != QueijoBrie && item.Nome != IngressosConcertoTuristas)
                {
                    if (item.Qualidade > 0)
                    {
                        if (item.Nome != DenteTarrasque)
                        {
                            item.Qualidade -= 1;

							if (item.Nome.Contains(ItemConjurado) && item.Qualidade > 0)
                            {
								item.Qualidade -= 1;
							}
						}

                    }
                }
                else
                {
                    if (item.Qualidade < 50)
                    {
                        item.Qualidade += 1;

                        if (item.Nome == IngressosConcertoTuristas)
                        {
                            if (item.PrazoParaVenda < 11)
                            {
                                if (item.Qualidade < 50)
                                {
                                    item.Qualidade += 1;
                                }
                            }

                            if (item.PrazoParaVenda < 6)
                            {
                                if (item.Qualidade < 50)
                                {
                                    item.Qualidade += 1;
                                }
                            }
                        }
                    }
                }

                if (item.Nome != DenteTarrasque)
                {
                    item.PrazoParaVenda -= 1;
                }

                if (item.PrazoParaVenda < 0)
                {
                    if (item.Nome != QueijoBrie)
                    {
                        if (item.Nome != "Ingressos para o concerto do Turisas")
                        {
                            if (item.Qualidade > 0)
                            {
                                if (item.Nome != DenteTarrasque)
                                {
                                    item.Qualidade -= 1;

									if (item.Nome.Contains(ItemConjurado) && item.Qualidade > 0)
                                    {
										item.Qualidade -= 1;
									}
								}
                            }
                        }
                        else
                        {
                            item.Qualidade -= item.Qualidade;
                        }
                    }
                    else
                    {
                        if (item.Qualidade < 50)
                        {
                            item.Qualidade += 1;
                        }
                    }
                }
            }
        }
    }
}
