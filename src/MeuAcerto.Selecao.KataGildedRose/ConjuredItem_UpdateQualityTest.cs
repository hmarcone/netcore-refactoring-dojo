﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class ConjuredItem_UpdateQualityTest
    {
        [Fact]
        public void ReduzindoQualidadeEmDois_Test()
        {
            var conjuredItem = new Item { Nome = "Bolo de Mana Conjurado", Qualidade = 20, PrazoParaVenda = 10 };
            var items = new List<Item> { conjuredItem };

            var gildedRose = new GildedRose(items);

            gildedRose.AtualizarQualidade();

            Assert.Equal(18, conjuredItem.Qualidade);
        }

        [Fact]
        public void ReduzindoQualidadeEmQuatro_Test()
        {
            var conjuredItem = new Item { Nome = "Bolo de Mana Conjurado", Qualidade = 20, PrazoParaVenda = 1 };
            var items = new List<Item> { conjuredItem };

            var gildedRose = new GildedRose(items);

            gildedRose.AtualizarQualidade(); //-2
            gildedRose.AtualizarQualidade(); //-4
            gildedRose.AtualizarQualidade(); //-4

            Assert.Equal(10, conjuredItem.Qualidade);
        }

        [Fact]
        public void VerificandoQualidadeEmZero_Test()
        {
            var conjuredItem = new Item { Nome = "Bolo de Mana Conjurado", Qualidade = 1, PrazoParaVenda = 10 };
            var items = new List<Item> { conjuredItem };

            var gildedRose = new GildedRose(items);

            gildedRose.AtualizarQualidade(); 
            
            Assert.Equal(0, conjuredItem.Qualidade);
        }

        [Fact]
        public void VerificandoQualidadeEmZero_ParaPrazoVendaZero_Test()
        {
            var conjuredItem = new Item { Nome = "Bolo de Mana Conjurado", Qualidade = 3, PrazoParaVenda = 0 };
            var items = new List<Item> { conjuredItem };

            var gildedRose = new GildedRose(items);

            gildedRose.AtualizarQualidade(); 

            Assert.Equal(0, conjuredItem.Qualidade);
        }

    }
}
