﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class NormalItem_UpdateQualityTest
    {
        [Fact]
        public void ExecutarSemErro_PassandoUmaColecaoVazia_Test()
        {
            var gildedRose = new GildedRose(new List<Item>());
            gildedRose.AtualizarQualidade();
        }

        [Fact]
        public void ValidarQualidadeBaixa_Test()
        {
            var itemNormal = new Item { Nome = "Item Normal", Qualidade = 20, PrazoParaVenda = 10 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);
            gildedRose.AtualizarQualidade();
            Assert.Equal(19, itemNormal.Qualidade);
        }

        [Fact]
        public void RealizarVendaAntesDoPrazo_Test()
        {
            var itemNormal = new Item { Nome = "Item Normal", Qualidade = 20, PrazoParaVenda = 10 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);
            gildedRose.AtualizarQualidade();
            Assert.Equal(9, itemNormal.PrazoParaVenda);
        }

        [Fact]
        public void BaixarQualidadeParaZero_Test()
        {
            var itemNormal = new Item { Nome = "Item Normal", Qualidade = 1, PrazoParaVenda = 10 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);

            gildedRose.AtualizarQualidade();
            gildedRose.AtualizarQualidade();

            Assert.Equal(0, itemNormal.Qualidade);
        }

        [Fact]
        public void ValidandoPrazoVendaNegativo_Test()
        {
            var itemNormal = new Item { Nome = "Item Normal", Qualidade = 50, PrazoParaVenda = 1 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);

            gildedRose.AtualizarQualidade();
            gildedRose.AtualizarQualidade();

            Assert.Equal(-1, itemNormal.PrazoParaVenda);
        }

        [Fact]
        public void QualidadeAumentadaEm2QuandoForMenorOuIgual10_Test()
        {
            var itemNormal = new Item { Nome = "Queijo Brie Envelhecido", Qualidade = 0 , PrazoParaVenda = 2 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);

            gildedRose.AtualizarQualidade(); //qualidade = 1
            gildedRose.AtualizarQualidade(); //qualidade = 2
            gildedRose.AtualizarQualidade(); //qualidade = 4

            Assert.Equal(4, itemNormal.Qualidade);
        }

        [Fact]
        public void QualidadeAumentadaEm3QuandoForMenorOuIgual5_Test()
        {
            var itemNormal = new Item { Nome = "Ingressos para o concerto do Turisas", Qualidade = 1, PrazoParaVenda = 5 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);

            gildedRose.AtualizarQualidade(); //qualidade = 4
            gildedRose.AtualizarQualidade(); //qualidade = 7
            gildedRose.AtualizarQualidade(); //qualidade = 10

            Assert.Equal(10, itemNormal.Qualidade);
        }

        [Fact]
        public void QualidadeVaiDiretoAzeroQuandoPrazoTiverPassado_Test()
        {
            var itemNormal = new Item { Nome = "Ingressos para o concerto do Turisas", Qualidade = 1, PrazoParaVenda = 2 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);
            var prazo = itemNormal.PrazoParaVenda;

            gildedRose.AtualizarQualidade(); //qualidade = 4 - Prazo = 1
            gildedRose.AtualizarQualidade(); //qualidade = 7 - Prazo = 0
            gildedRose.AtualizarQualidade(); //qualidade = 0 - Prazo = 0

            Assert.Equal(0, itemNormal.Qualidade);
        }

        [Fact]
        //Quando a data de venda do item tiver passado, a (Qualidade) do item diminui duas vezes mais rápido.
        public void DiminuindoQualidadeDuasVezes_PrazaoParaVendaZero_Test()
        {
            var itemNormal = new Item { Nome = "Item Normal", Qualidade = 50, PrazoParaVenda = 1 };
            var itens = new List<Item> { itemNormal };
            var gildedRose = new GildedRose(itens);

            gildedRose.AtualizarQualidade(); //-1
            gildedRose.AtualizarQualidade(); //-2
            gildedRose.AtualizarQualidade(); //-2

            Assert.Equal(45, itemNormal.Qualidade);
        }
    }
}
